FROM golang:alpine AS builder
RUN apk --no-cache add ca-certificates
WORKDIR /go/src/go-gentoo
COPY . /go/src/go-gentoo
RUN go get github.com/go-pg/pg/v9
RUN go get github.com/mcuadros/go-version
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o bin .

FROM scratch
WORKDIR /go/src/go-gentoo
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /go/src/go-gentoo/assets /go/src/go-gentoo/assets
COPY --from=builder /go/src/go-gentoo/bin /go/src/go-gentoo/bin
COPY --from=builder /go/src/go-gentoo/pkg /go/src/go-gentoo/pkg
COPY --from=builder /go/src/go-gentoo/web /go/src/go-gentoo/web
ENTRYPOINT ["/go/src/go-gentoo/bin/go-gentoo", "--serve"]
