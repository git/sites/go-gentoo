function togglePrefix(){
    if(document.getElementById('prefix').value == ''){
        document.getElementById('token').value = "";
        document.getElementById('token').disabled = true;
        document.getElementById('token').placeholder = "Only available for prefix != '/'";
        document.getElementById('index').value = "no";
        document.getElementById('index').disabled = true;
    }else{
        document.getElementById('token').disabled = false;
        document.getElementById('token').placeholder = "Automatically generated if empty";
        document.getElementById('index').disabled = false;
    }
}
