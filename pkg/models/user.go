// Contains the model of the application data

package models

import (
	"encoding/xml"
	"io/ioutil"
	"net/http"
	"strings"
)

type User struct {
	Email    string `pg:",pk"`
	RealName string
	UserName string
	Projects []string
}

func (u *User) IsAdmin() bool {
	for _, project := range u.Projects {
		if project == "infra" {
			return true
		}
	}
	return false
}

func (u *User) ComputeProjects() error {
	projects, err := parseProjectList()

	if err != nil {
		return err
	}

	for _, project := range projects.Projects {
		for _, member := range project.Members {
			if member.Email == u.Email {
				abbreviation := strings.ReplaceAll(project.Email, "@gentoo.org", "")
				u.Projects = append(u.Projects, abbreviation)
			}
		}
	}

	return nil
}

// parseQAReport gets the xml from qa-reports.gentoo.org and parses it
func parseProjectList() (ProjectList, error) {
	resp, err := http.Get("https://api.gentoo.org/metastructure/projects.xml")
	if err != nil {
		return ProjectList{}, err
	}
	defer resp.Body.Close()
	xmlData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return ProjectList{}, err
	}
	var projectList ProjectList
	xml.Unmarshal(xmlData, &projectList)
	return projectList, err
}
