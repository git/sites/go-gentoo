// Contains the model of the application data

package models

type Link struct {
	Id         int `pg:",pk"`
	Prefix     string
	URLToken   string
	ShortLink  string `pg:",unique"`
	TargetLink string
	UserEmail  string
	Hits       int
}
