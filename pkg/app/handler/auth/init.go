package auth

import (
	"context"
	"encoding/gob"
	"github.com/coreos/go-oidc"
	"github.com/gorilla/sessions"
	"go-gentoo/pkg/config"
	"go-gentoo/pkg/models"
	"golang.org/x/oauth2"
)

var (
	Oauth2Config oauth2.Config
	Verifier     *oidc.IDTokenVerifier
	Ctx          context.Context
	CookieStore  *sessions.CookieStore
)

func Init() {
	gob.Register(&models.User{})

	Ctx = context.Background()
	provider, err := oidc.NewProvider(Ctx, config.OIDConfigURL())
	if err != nil {
		panic(err)
	}

	CookieStore = sessions.NewCookieStore([]byte(config.SessionSecret()))

	// Configure an OpenID Connect aware OAuth2 client.
	Oauth2Config = oauth2.Config{
		ClientID:     config.OIDClientID(),
		ClientSecret: config.OIDClientSecret(),
		RedirectURL:  config.ApplicationURL() + "/auth/callback",
		// Discovery returns the OAuth2 endpoints.
		Endpoint: provider.Endpoint(),
		// "openid" is a required scope for OpenID Connect flows.
		Scopes: []string{oidc.ScopeOpenID, "profile", "email"},
	}

	oidcConfig := &oidc.Config{
		ClientID: config.OIDClientID(),
	}
	Verifier = provider.Verifier(oidcConfig)

}
