package auth

import (
	"go-gentoo/pkg/config"
	"go-gentoo/pkg/models"
	"net/http"
)

func IsValidUser(w http.ResponseWriter, r *http.Request) bool {
	session, err := CookieStore.Get(r, config.SessionStoreKey())

	if err != nil {
		return false
	}

	if token, ok := session.Values["idToken"].(string); ok {
		_, err = Verifier.Verify(Ctx, token)
		return err == nil
	}

	return false
}

func GetUser(w http.ResponseWriter, r *http.Request) *models.User {
	session, err := CookieStore.Get(r, config.SessionStoreKey())
	if err != nil {
		return nil
	}
	user := session.Values["user"].(*models.User)
	err = user.ComputeProjects()
	if err != nil {
		return nil
	}
	return user
}
