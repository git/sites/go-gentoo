// Used to show the landing page of the application

package admin

import (
	"html/template"
	"net/http"
)

// Show renders a template to show the landing page of the application
func Show(w http.ResponseWriter, r *http.Request) {

	templates := template.Must(
		template.Must(
			template.New("Show").
				Funcs(getFuncMap()).
				ParseGlob("web/templates/layout/*.tmpl")).
			ParseGlob("web/templates/admin/*.tmpl"))

	templates.ExecuteTemplate(w, "show.tmpl", getPageData(w, r))
}
