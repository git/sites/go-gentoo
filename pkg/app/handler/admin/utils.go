// miscellaneous utility functions used for the landing page of the application

package admin

import (
	"crypto/md5"
	"fmt"
	"go-gentoo/pkg/app/handler/auth"
	"go-gentoo/pkg/database"
	"go-gentoo/pkg/models"
	"html/template"
	"net/http"
	"strings"
)

func getPageData(w http.ResponseWriter, r *http.Request) interface{} {
	user := auth.GetUser(w, r)
	return struct {
		Tab       string
		User      *models.User
		UserLinks []models.Link
	}{
		Tab:       "admin",
		User:      user,
		UserLinks: getAllLinks(),
	}
}

func getFuncMap() template.FuncMap {
	return template.FuncMap{
		"gravatar":      emailToGravater,
		"replaceAll":    strings.ReplaceAll,
		"getPrefixList": getPrefixList,
	}
}

func emailToGravater(email string) string {
	return "https://www.gravatar.com/avatar/" + fmt.Sprintf("%x", md5.Sum([]byte(email)))
}

func getAllLinks() []models.Link {
	var links []models.Link
	database.DBCon.Model(&links).
		Select()
	return links
}

func getPrefixList(links []models.Link) []string {
	prefixMap := make(map[string]bool)
	var prefixList []string
	for _, link := range links {
		if link.Prefix != "" {
			prefixMap[link.Prefix] = true
		}
	}
	for key, _ := range prefixMap {
		prefixList = append(prefixList, key)
	}
	return prefixList
}
