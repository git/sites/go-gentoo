package links

import (
	"crypto/md5"
	"fmt"
	"github.com/catinello/base62"
	"go-gentoo/pkg/app/handler/auth"
	"go-gentoo/pkg/config"
	"go-gentoo/pkg/database"
	"go-gentoo/pkg/models"
	"html/template"
	"net/http"
	"net/url"
	"strings"
)

const (
	MAX_TRY           = 10
	MAX_TOKEN_LENGTH  = 75
)

func getPageData(w http.ResponseWriter, r *http.Request, tab string) interface{} {
	user := auth.GetUser(w, r)
	return struct {
		Tab       string
		User      *models.User
		UserLinks []models.Link
	}{
		Tab:       tab,
		User:      user,
		UserLinks: getLinks(user),
	}
}

func getCreatePageData(w http.ResponseWriter, r *http.Request, shortlink, target string) interface{} {
	return struct {
		Tab       string
		User      *models.User
		ShortLink string
		Target    string
	}{
		Tab:       "new",
		User:      auth.GetUser(w, r),
		ShortLink: config.ApplicationURL() + shortlink,
		Target:    target,
	}
}

func getFuncMap() template.FuncMap {
	return template.FuncMap{
		"gravatar":   emailToGravater,
		"replaceAll": strings.ReplaceAll,
	}
}

func emailToGravater(email string) string {
	return "https://www.gravatar.com/avatar/" + fmt.Sprintf("%x", md5.Sum([]byte(email)))
}

func getLinks(user *models.User) []models.Link {
	var links []models.Link
	database.DBCon.Model(&links).
		Where("user_email = '" + user.Email + "'").
		WhereOr(createQuery(user)).
		Select()
	return links
}

func createQuery(user *models.User) string {
	var queryParts []string
	for _, project := range user.Projects {
		queryParts = append(queryParts, "prefix = '"+project+"'")
	}
	return strings.Join(queryParts, " OR ")
}

func isValidPrefix(user *models.User, prefix string) bool {
	return prefixIsNotReserved(prefix) && (contains(user.Projects, prefix) || prefix == "" || prefix == user.UserName)
}

func prefixIsNotReserved(prefix string) bool {
	return prefix != "auth" && prefix != "assets" && prefix != "links" && prefix != "admin"
}

func isValidToken(token string) bool {
	for _, char := range token {
		if !strings.Contains(config.ValidURLTokenChars(), strings.ToLower(string(char))) {
			return false
		}
	}
	return len(token) < MAX_TOKEN_LENGTH
}

func isValidTarget(target string) bool {
	_, err := url.ParseRequestURI(target)
	return err == nil
}

func getLatestId() int {

	var links []models.Link
	database.DBCon.Model(&links).
		Order("id DESC").
		Limit(1).
		Select()

	if len(links) != 1 {
		return 4000
	}

	return links[0].Id
}

func createShortURL(prefix, token, target, username string, setIndex bool, try int) (string, bool) {
	var shortlink string
	id := getLatestId() + 1
	if token == "" && (!setIndex || prefix == "") {
		token = base62.Encode(id)
	}
	if prefix != "" {
		shortlink = "/" + prefix + "/" + token
	} else {
		shortlink = "/" + token
	}

	if len(getLink(shortlink)) > 0 && try <= MAX_TRY {
		return createShortURL(prefix, token, target, username, setIndex, try+1)
	}

	err := database.DBCon.Insert(&models.Link{
		Id:         id,
		Prefix:     prefix,
		URLToken:   token,
		ShortLink:  shortlink,
		TargetLink: target,
		UserEmail:  username,
		Hits:       0,
	})
	return shortlink, err == nil
}

func getLink(shortlink string) []models.Link {
	var links []models.Link
	database.DBCon.Model(&links).
		Where("short_link = ?", shortlink).
		Select()
	return links
}

func contains(list []string, value string) bool {
	for _, v := range list {
		if v == value {
			return true
		}
	}
	return false
}
