// Used to show the landing page of the application

package index

import (
	"go-gentoo/pkg/database"
	"go-gentoo/pkg/models"
	"net/http"
)

// Show renders a template to show the landing page of the application
func Handle(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		redirect(w, r)
		return
	}
	http.Redirect(w, r, "/links/create", http.StatusFound)
}

func redirect(w http.ResponseWriter, r *http.Request) {
	links := getLink(r.URL.Path)
	if len(links) != 1 {
		http.Error(w, "Not found", http.StatusNotFound)
	} else {
		link := links[0]
		link.Hits++
		database.DBCon.Model(&link).WherePK().Update()
		http.Redirect(w, r, links[0].TargetLink, http.StatusFound)
	}
}

func getLink(shortlink string) []models.Link {
	var links []models.Link
	database.DBCon.Model(&links).
		Where("short_link = ?", shortlink).
		Select()
	return links
}
